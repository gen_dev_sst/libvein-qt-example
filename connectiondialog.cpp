#include "connectiondialog.h"
#include "ui_connectiondialog.h"

ConnectionDialog::ConnectionDialog(QWidget *qobjParent) :
  QDialog(qobjParent),
  ui(new Ui::ConnectionDialog)
{
  ui->setupUi(this);
}

ConnectionDialog::~ConnectionDialog()
{
  delete ui;
}

QString ConnectionDialog::getIpAddress()
{
  return ui->leIpAddress->text();
}

quint16 ConnectionDialog::getPort()
{
  return ui->lePort->text().toUShort();
}
