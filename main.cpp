#include "mdimainwindow.h"

#include <veintcpcontroller.h>
#include <veinpeer.h>

#include <QApplication>




int main(int argc, char *argv[])
{
  QApplication a(argc, argv);
  MdiMainWindow w;
  w.show();

  VeinTcpController *vtc = new VeinTcpController(&a);

  w.setHub(vtc->getLocalHub());

  QObject::connect(vtc, &VeinTcpController::sigPeerAvailable, &w, &MdiMainWindow::onPeerRegistered);
  QObject::connect(&w, &MdiMainWindow::sigStartConnection, vtc, &VeinTcpController::connectToServer);

  qsrand(a.applicationPid());
  quint16 randomPort = 16000;//(qrand()%32000)+1024;
  vtc->startService(randomPort);
  
  return a.exec();
}
