#include "datawidget.h"
#include "ui_datawidget.h"

#include <veinentity.h>
#include <veinpeer.h>
#include <veinhub.h>

#include <QDebug>

DataWidget::DataWidget(QWidget *qobjParent) :
  QWidget(qobjParent),
  ui(new Ui::DataWidget)
{
  cVal=0;
  pVal=0;
  sVal=0;
  rVal=0;

  ui->setupUi(this);
}

DataWidget::~DataWidget()
{
  delete ui;
}

void DataWidget::setPeer(VeinPeer *peer)
{
  dataPeer  = peer;

  dataEnt = dataPeer->getEntityByName("dataEnt");
  textEnt = dataPeer->getEntityByName("textEnt");
  VeinEntity *slTest = dataPeer->getEntityByName("stringList");
  if(slTest)
  {
    slTest->setValue(QStringList() << "test" << "string" << "list",dataPeer);
    slTest->modifiersAdd(VeinEntity::MOD_CONST);
    qDebug() << "TEST" << slTest->getValue();
  }

  dataEnt->setValue(0,dataPeer);
  textEnt->setValue("",dataPeer);

  ui->sbValue->setValue(dataEnt->getValue().toInt());
  ui->teText->setText(textEnt->getValue().toString());

  if(dataPeer->isRemote() && dataEnt->getModifiers().contains(VeinEntity::MOD_READONLY))
  {
    ui->sbValue->setEnabled(false);
    ui->hslValue->setEnabled(false);
  }

  //only connect one of the  Data ui elements to not receive signals echoes
  connect(ui->sbValue, SIGNAL(valueChanged(int)), this, SLOT(onValueChanged(int)));
  connect(ui->teText,SIGNAL(textChanged()),this,SLOT(onTextChanged()));

  connect(dataEnt, &VeinEntity::sigValueChanged, this, &DataWidget::onExternValueChanged);
  connect(textEnt, &VeinEntity::sigValueChanged, this, &DataWidget::onExternTextChanged);

  connect(peer->getHub(),SIGNAL(sigSendProtobufMessage(google::protobuf::Message*)),this, SLOT(counterSentUp()));


  //ui->lPeerUuid->setText(peer->getPeerUuid().toString());
  ui->lHubUuid->setText(peer->getHub()->getUuid().toString());
}

void DataWidget::tmpDebug(QVariant value)
{
  Q_UNUSED(value)
  //qDebug() << dataPeer->getPeerUuid().toString() <<"Updated:" << value;
}

void DataWidget::onValueChanged(int value)
{
  if(value!=dataEnt->getValue())
  {
    counterChangedUp();
    dataEnt->setValue(value,dataPeer);
  }
}

void DataWidget::onTextChanged()
{
  if(ui->teText->toPlainText()!=textEnt->getValue())
  {
    counterChangedUp();
    textEnt->setValue(ui->teText->toPlainText(),dataPeer);
  }
}

void DataWidget::onExternValueChanged(QVariant value)
{
  counterProtobufUp();
  int tmpInt = value.toInt();
  if(tmpInt != ui->hslValue->value())
  {
    //tmpDebug(value);
    ui->hslValue->setValue(tmpInt);
  }
}

void DataWidget::onExternTextChanged(QVariant text)
{
  counterProtobufUp();
  QString tmpString = text.toString();
  if(ui->teText->toPlainText() != tmpString)
  {
    //tmpDebug(text);
    ui->teText->setText(tmpString);
  }
}

void DataWidget::counterChangedUp()
{
  cVal++;
  ui->lUpdatesValue->setText(QString::number(cVal));
}

void DataWidget::counterProtobufUp()
{
  pVal++;
  ui->lProtobufValue->setText(QString::number(pVal));
}

void DataWidget::counterSentUp()
{
  sVal++;
  ui->lSentValue->setText(QString::number(sVal));
}

void DataWidget::counterReceivedUp()
{
  rVal++;
  ui->lReceivedValue->setText(QString::number(rVal));
}
