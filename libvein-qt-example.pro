#-------------------------------------------------
#
# Project created by QtCreator 2013-11-13T11:06:28
#
#-------------------------------------------------

exists( ../include/project-paths.pri ) {
  include(../include/project-paths.pri)
}
else:exists( ../../project-paths.pri ) {
  include(../../project-paths.pri)
}

QT       += core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = libvein-qt-example
TEMPLATE = app


SOURCES += main.cpp\
        mdimainwindow.cpp \
    datawidget.cpp \
    connectiondialog.cpp

HEADERS  += mdimainwindow.h \
    datawidget.h \
    connectiondialog.h

FORMS    += mdimainwindow.ui \
    datawidget.ui \
    connectiondialog.ui

QMAKE_CXXFLAGS += -Wall -Wshadow

INCLUDEPATH += $$VEIN_INCLUDEDIR
LIBS += $$VEIN_LIBDIR -lvein-qt
INCLUDEPATH += $$PROTONET_INCLUDEDIR
LIBS += $$PROTONET_LIBDIR -lproto-net-qt
INCLUDEPATH += $$VEIN_PROTOBUF_INLCUDEDIR
LIBS += $$VEIN_PROTOBUF_LIBDIR -lvein-qt-protobuf
INCLUDEPATH += $$VEIN_TCP_INCLUDEDIR
LIBS += $$VEIN_TCP_LIBDIR -lvein-tcp-overlay

LIBS += -lprotobuf

target.path = /usr/bin
INSTALLS += target

