#ifndef DATAWIDGET_H
#define DATAWIDGET_H

#include <QWidget>
#include <QVariant>

class VeinEntity;
class VeinPeer;

namespace Ui {
  class DataWidget;
}

class DataWidget : public QWidget
{
  Q_OBJECT
  
public:
  explicit DataWidget(QWidget *qobjParent = 0);
  ~DataWidget();
  
  void setPeer(VeinPeer *peer);

public slots:
  void tmpDebug(QVariant value);

protected slots:
  void onValueChanged(int value);
  void onTextChanged();

  void onExternValueChanged(QVariant value);
  void onExternTextChanged(QVariant text);

  void counterChangedUp();
  void counterProtobufUp();
  void counterSentUp();
  void counterReceivedUp();
private:
  Ui::DataWidget *ui;

  VeinPeer *dataPeer;
  VeinEntity *dataEnt;
  VeinEntity *textEnt;

  int cVal, pVal, sVal, rVal;
};

#endif // DATAWIDGET_H
