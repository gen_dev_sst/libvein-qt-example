#ifndef MDIMAINWINDOW_H
#define MDIMAINWINDOW_H

#include <QMainWindow>
#include <QHostAddress>

namespace Ui {
  class MdiMainWindow;
}

class DataWidget;
class VeinHub;
class VeinPeer;

class MdiMainWindow : public QMainWindow
{
  Q_OBJECT
  
public:
  explicit MdiMainWindow(QWidget *qobjParent = 0);
  ~MdiMainWindow();
  
  void setHub(VeinHub *vHub);

signals:
  void sigReady();
  void sigStartConnection(QString ipAddress, quint16 port);

public slots:
  void onPeerRegistered(VeinPeer *vPeer);
  void onActionConnectToServer();

private:
  void setupSigSlots();
  void setupSubwindow();

  Ui::MdiMainWindow *ui;
  DataWidget *dWidget;
  QList<DataWidget*> foreignWidgets;
  VeinHub *lHub;
  VeinPeer *lPeer;
};

#endif // MDIMAINWINDOW_H
