#ifndef CONNECTIONDIALOG_H
#define CONNECTIONDIALOG_H

#include <QDialog>

namespace Ui {
  class ConnectionDialog;
}

class ConnectionDialog : public QDialog
{
  Q_OBJECT
  
public:
  explicit ConnectionDialog(QWidget *qobjParent = 0);
  ~ConnectionDialog();

  QString getIpAddress();
  quint16 getPort();
  
private:
  Ui::ConnectionDialog *ui;
};

#endif // CONNECTIONDIALOG_H
