#include "mdimainwindow.h"
#include "ui_mdimainwindow.h"

#include "datawidget.h"
#include "connectiondialog.h"

#include <veinhub.h>
#include <veinpeer.h>
#include <veinentity.h>

MdiMainWindow::MdiMainWindow(QWidget *qobjParent) :
  QMainWindow(qobjParent),
  ui(new Ui::MdiMainWindow)
{
  ui->setupUi(this);
  setCentralWidget(ui->mdiArea);
  setupSigSlots();

}

MdiMainWindow::~MdiMainWindow()
{
  delete ui;
}

void MdiMainWindow::setHub(VeinHub *vHub)
{
  lHub = vHub;
  lHub->setUuid(QUuid::createUuid());


  lPeer = lHub->peerAdd("LocalPeer");

  VeinEntity * tmpEnt = lPeer->dataAdd("dataEnt");
  tmpEnt->modifiersAdd(VeinEntity::MOD_NOECHO);
  tmpEnt->modifiersAdd(VeinEntity::MOD_READONLY);
  tmpEnt = lPeer->dataAdd("textEnt");
  tmpEnt->modifiersAdd(VeinEntity::MOD_NOECHO);
  tmpEnt = lPeer->dataAdd("stringList");



  //this is a circular connection, so better not bounce changes back and forth all the time
  //dataEnt->modifiersAdd(VeinEntity::MOD_NOECHO);
  //textEnt->modifiersAdd(VeinEntity::MOD_NOECHO);


  //qDebug() << "0 HUBUUID:" << lHub->getUuid();

  setupSubwindow();
}

void MdiMainWindow::onPeerRegistered(VeinPeer *vPeer)
{
  DataWidget *tmpW = new DataWidget(this);
  foreignWidgets.append(tmpW);
  ui->mdiArea->addSubWindow(tmpW);

  //qDebug() << "7 onPeerRegistered(VeinPeer *vPeer) Uuid:" << vPeer->getPeerUuid();
  tmpW->setPeer(vPeer);
  tmpW->show();
}

void MdiMainWindow::onActionConnectToServer()
{
  ConnectionDialog *pDialog = new ConnectionDialog(this);

  if(pDialog->exec()==QDialog::Accepted)
  {
    sigStartConnection(pDialog->getIpAddress(), pDialog->getPort());
  }
}

void MdiMainWindow::setupSigSlots()
{
  connect(ui->actionConnectToServer, SIGNAL(triggered()), this, SLOT(onActionConnectToServer()));
}

void MdiMainWindow::setupSubwindow()
{
  dWidget = new DataWidget(this);
  ui->mdiArea->addSubWindow(dWidget);

  dWidget->setPeer(lPeer);
  dWidget->show();
}
